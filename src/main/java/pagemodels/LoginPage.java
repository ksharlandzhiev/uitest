package pagemodels;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageObject{

    @FindBy(css="input#username")
    private WebElement username;

    @FindBy(css="input#password")
    private WebElement password;

    @FindBy(xpath=".//footer/button")
    private WebElement loginButton;

    public LoginPage (WebDriver driver){
        super(driver);
    }

    public boolean isInitialized() {
        return username.isDisplayed();
    }

    public void enterUsername(String firstName) {
        this.username.clear();
        this.username.sendKeys(firstName);
    }

    public void enterPassword(String password) {
        this.password.clear();
        this.password.sendKeys(password);
    }

    public DashboardPage submit(){
        loginButton.click();
        return new DashboardPage(driver);
    }

}
