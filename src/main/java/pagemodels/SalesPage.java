package pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SalesPage extends PageObject {

    @FindBy(xpath = "//div[3]/div/div/div[1]")
    private WebElement salesLogo;

    @FindBy(css = ".css-12je7x4-heading-heading--giga.erjtaf20")
    private WebElement noTransactionsElement;


    public SalesPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return salesLogo.isDisplayed();
    }

    public boolean isEmpty() {
        //needed to deal with loading of the image
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".css-12je7x4-heading-heading--giga.erjtaf20")));
        return noTransactionsElement.isDisplayed();
    }

    public String noTransactions(){
        return noTransactionsElement.getText();
    }


}
