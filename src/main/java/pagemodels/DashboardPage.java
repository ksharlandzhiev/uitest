package pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage extends PageObject {

    @FindBy(linkText = "Dashboard")
    private WebElement Dashboard;

    @FindBy(css = "img[alt='Empty Overview']")
    private WebElement imgEmptyOverview;

    @FindBy(linkText = "Sales")
    private WebElement Sales;

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return Dashboard.isDisplayed();
    }

    public boolean isEmpty() {
        //needed to deal with loading of the image
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img[alt='Empty Overview']")));
        return imgEmptyOverview.isDisplayed();
    }

    public SalesPage gotoSales(){
        Sales.click();
        return new SalesPage(driver);
    }

}
