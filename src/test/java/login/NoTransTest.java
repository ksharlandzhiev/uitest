package login;

import org.junit.Test;
import pagemodels.DashboardPage;
import pagemodels.LoginPage;
import pagemodels.SalesPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NoTransTest extends FunctionalTest {

        @Test
        public void logIn(){
            driver.get("https://me.sumup.com/login");

            LoginPage loginPage = new LoginPage(driver);
            assertTrue(loginPage.isInitialized());

            loginPage.enterUsername("ksharlandzhiev@gmail.com");
            loginPage.enterPassword("Limon4eto");

            DashboardPage dashboardPage = loginPage.submit();
            assertTrue(dashboardPage.isInitialized());

            assertTrue(dashboardPage.isEmpty());

            SalesPage salesPage = dashboardPage.gotoSales();
            assertTrue(salesPage.isInitialized());

            assertEquals("We couldn’t find anything that matches your search.", salesPage.noTransactions());



        }

}
